//
//  ViewController.swift
//  G57L8MultipleView
//
//  Created by Nikita Kostash on 19.10.17.
//  Copyright © 2017 Nikita Kostash. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var myLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print(#function)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        checkLabel()
        print(#function)
    }
    override func viewWillAppear( _ animated: Bool) {
        super.viewWillAppear(animated)
        checkLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    func checkLabel() {
        if myLabel == nil {
            print("myLabel == nil")
        }
        else {
            print("myLabel != nil")
        }
    }

}

